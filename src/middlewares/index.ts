import Koa from "koa";
import cors from "koa-cors";
import getRouter from "../router";
import logger from './logger';



const corsConfig= {
  credentials: true, // 允许使用身份验证信息
  origin: "*",
  methods: ["GET", "POST", "PUT"], // 允许的 HTTP 方法
  headers: ["Content-Type", "Authorization"], // 允许的头部信息
}


export default function setMiddlewares(app: Koa) {
  const router = getRouter();
  app.use(cors(corsConfig));
  app.use(router.routes());
  app.use(router.allowedMethods()); //我们将路由中间件注册到应用程序中，并使用 allowedMethods() 方法处理不支持的 HTTP 方法
  app.use(async (ctx, next) => {
    console.log(`Received ${ctx.method} request for ${ctx.url}`);
    try {
      await next();
    } catch (err) {
      logger.error(err);
      ctx.status = err.status || 500;
      ctx.body = err.message;
    }
  });

  // 响应所有未处理的请求
  app.use(async (ctx) => {
    ctx.status = 404;
    ctx.body = `${ctx.method} request for ${ctx.url} is Not found`;
  });
}
